﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float dashSpeed = 20f;
    public float dashWait = 1f;
    float dashTimer = 0f;
    float freezeCooldown = 10f;
    private float nextFreezeTime = 0;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    WaypointPatrol[] allPatrols;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        allPatrols = FindObjectsOfType<WaypointPatrol>();
    }

    private void Update()
    {
        if (Time.time > nextFreezeTime)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                FreezeGhost();
                nextFreezeTime = Time.time + freezeCooldown;
            }
        }
            if (dashTimer > 0f)
            {
                dashTimer -= Time.deltaTime;
            }
        
    }

    void FreezeGhost()
    {
        for (int i = 0; i <allPatrols.Length; i++)
        {
            if (allPatrols[i] != null)
            {
                allPatrols[i].navMeshAgent.isStopped = true;
            }
        }
        CancelInvoke("EndFreeze");
        Invoke("EndFreeze", 2f);
    }
    void EndFreeze()
    {
        for (int i = 0; i < allPatrols.Length; i++)
        {
            if (allPatrols[i] != null)
            {
                allPatrols[i].navMeshAgent.isStopped = false;
            }
        }
    }


    void FixedUpdate()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        if (m_Movement.magnitude > 0f && dashTimer <= 0f)
        {
            dashTimer = dashWait;
            m_Rigidbody.velocity += m_Movement * dashSpeed;
            transform.rotation = Quaternion.LookRotation(m_Movement);
        }

        /*
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
        */
    }
    /*
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    */
}
